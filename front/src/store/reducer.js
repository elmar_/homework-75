import {
    CHANGING,
    FETCH_MESSAGE_ERROR,
    FETCH_MESSAGE_REQUEST,
    FETCH_MESSAGE_SUCCESS_DECODED,
    FETCH_MESSAGE_SUCCESS_ENCODE
} from "./actions";

const initialState = {
    decoded: '',
    password: '',
    messageLoading: false,
    error: false,
    encoded: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGING:
            const name = action.name;
            return {...state, [name]: action.value};
        case FETCH_MESSAGE_REQUEST:
            return {...state, messageLoading: true};
        case FETCH_MESSAGE_SUCCESS_ENCODE:
            return {...state, encoded: action.encoded, messageLoading: false};
        case FETCH_MESSAGE_SUCCESS_DECODED:
            return {...state, decoded: action.decoded, messageLoading: false};
        case FETCH_MESSAGE_ERROR:
            return {...state, messageLoading: false, error: true};
        default:
            return state;
    }
};

export default reducer;