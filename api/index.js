const express = require('express');
const cors = require('cors');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();

app.use(cors());
app.use(express.json());

const port = 8000;

app.post('/encode', (req, res) => {
    const response = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    res.send({encoded: response});
});

app.post('/decode', (req, res) => {
    const response = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send({decoded: response});
});

app.listen(port, () => {
    console.log('We are on ' + port);
});

