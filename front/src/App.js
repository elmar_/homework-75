import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {changing, fetchMessageDecode, fetchMessageEncode} from "./store/actions";
import Spinner from "./components/Spinner/Spinner";
import './App.css';

const App = () => {
    const dispatch = useDispatch();
    const loading = useSelector(state => state.messageLoading);
    const decoded = useSelector(state => state.decoded);
    const encoded = useSelector(state => state.encoded);
    const password = useSelector(state => state.password);


    const changingText = e => {
        const {name, value} = e.target;

        dispatch(changing(name, value));
    };

    const fetchDataEncode = () => {
        const data = {
            message: decoded,
            password: password
        }
        if (data.message && data.password) {
            dispatch(fetchMessageEncode(data));
        } else {
            alert('Введите данные');
        }
    };

    const fetchDataDecode = () => {
        const data = {
            message: encoded,
            password: password
        }
        if (data.message && data.password) {
            dispatch(fetchMessageDecode(data));
        } else {
            alert('Введите данные');
        }
    };

    let all = (
        <div className="container">
            <p>Decoded Message</p>
            <textarea name="decoded" onChange={changingText} value={decoded} />
            <p>Password</p>
            <div>
                <input type="text" name="password" onChange={changingText} value={password} className="pasInpt" />
                <button onClick={fetchDataEncode} className="encodeBtn">encode</button>
                <button onClick={fetchDataDecode}>decode</button>
            </div>
            <p>Encoded Message</p>
            <textarea name="encoded" onChange={changingText} value={encoded} />
        </div>
    );

    if (loading) {
        all = <Spinner />;
    }

    return (
        <>
            {all}
        </>
    );
};

export default App;