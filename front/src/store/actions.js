import axiosMessage from "../axiosMessage";

export const FETCH_MESSAGE_REQUEST = 'FETCH_MESSAGE_REQUEST';
export const FETCH_MESSAGE_SUCCESS_ENCODE = 'FETCH_MESSAGE_SUCCESS_ENCODE';
export const FETCH_MESSAGE_ERROR = 'FETCH_MESSAGE_REQUEST';

export const FETCH_MESSAGE_SUCCESS_DECODED = 'FETCH_MESSAGE_SUCCESS_DECODED';
export const fetchMessageSuccessDecoded = decoded => ({type: FETCH_MESSAGE_SUCCESS_DECODED, decoded});


export const CHANGING = 'CHANGING';


export const changing = (name, value) => ({type: CHANGING, name, value});

export const fetchMessageRequest = () => ({type: FETCH_MESSAGE_REQUEST});
export const fetchMessageSuccessEncoded = encoded => ({type: FETCH_MESSAGE_SUCCESS_ENCODE, encoded});
export const fetchMessageError = () => ({type: FETCH_MESSAGE_ERROR});

export const fetchMessageEncode = data => {
    return async dispatch => {
        try {
            dispatch(fetchMessageRequest());
            const response = await axiosMessage.post('/encode', data);
            console.log(response)
            dispatch(fetchMessageSuccessEncoded(response.data.encoded));
        } catch (e) {
            dispatch(fetchMessageError());
        }
    };
};

export const fetchMessageDecode = data => {
    return async dispatch => {
        try {
            dispatch(fetchMessageRequest());
            const response = await axiosMessage.post('/decode', data);
            console.log(response.data.decoded)
            dispatch(fetchMessageSuccessDecoded(response.data.decoded));
        } catch (e) {
            dispatch(fetchMessageError());
        }
    };
};
